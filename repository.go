package sonicdb

import (
	"database/sql"
	"fmt"
	"reflect"
	"strings"
)

// Scalar holds a scalar value of a database query, i.e. a count on a table
type Scalar struct {
	N int64 `db:"n"`
}

// Repository interface
type Repository interface {
	Get(sql string) ([]interface{}, error)
	GetAndBind(sql string, i interface{}) ([]interface{}, error)
	View(from string, criteria []Clause, selectors ...string) ([]interface{}, error)
	Find(i interface{}, id int64) (interface{}, error)
	FindAll(i interface{}) ([]interface{}, error)
	FindBy(i interface{}, criteria []Clause) ([]interface{}, error)
	FindByOrdered(i interface{}, criteria []Clause, ordering map[string]string) ([]interface{}, error)
	FindOneBy(i interface{}, criteria []Clause) (interface{}, error)
	FindOneByOrdered(i interface{}, criteria []Clause, ordering map[string]string) (interface{}, error)
	First(i interface{}, criteria []Clause) (interface{}, error)
	Count(i interface{}, criteria []Clause) (int64, error)
	Save(i interface{}) error
	Update(i interface{}) error
	UpdateBy(i interface{}, criteria []Clause) (int64, error)
	UpdateFields(i interface{}, fields []*Field) error
	DeleteBy(i interface{}, criteria []Clause) (int64, error)
	OneToOne(rv reflect.Value, c interface{}, join string) error
	OneToMany(parent reflect.Value, child interface{}, mappedBy string) error
	Relations(parent reflect.Value) interface{}
	MarshalObject(i interface{}) (interface{}, error)
}

// Database holds a reference to the SQL connection
type Database struct {
	*sql.DB
}

// NewDB returns a new database
func NewDB(dbSecrets DbSecrets) (*Database, error) {
	db, err := Open(dbSecrets)
	if err != nil {
		return nil, err
	}

	return &Database{
		DB: db,
	}, nil
}

// Get results from database with a custom built SQL string
func (db *Database) Get(sql string) ([]interface{}, error) {
	return doSelectView(db.DB, sql)
}

// GetAndBind results from database and bind it to the struct
func (db *Database) GetAndBind(sql string, i interface{}) ([]interface{}, error) {
	return doSelect(db.DB, sql, i)
}

// View returns a resultset for a custom query
func (db *Database) View(from string, criteria []Clause, selectors ...string) ([]interface{}, error) {
	qb := NewQuery(SelectStmt)
	qb.setTableName(from)

	// if no selectors
	if len(selectors) == 0 {
		selectors = append(selectors, "*")
	}

	qb.Select(selectors...)

	var q string
	if criteria != nil {
		q = buildCriteria(qb, criteria)
	} else {
		q = qb.getSQL()
	}

	return doSelectView(db.DB, q)
}

// Find a record in the database based on an interface and an id
func (db *Database) Find(i interface{}, id int64) (interface{}, error) {
	pk, _ := GetPrimaryKey(reflect.ValueOf(i))
	if pk == "" {
		return nil, fmt.Errorf("no primary key found")
	}

	return db.FindOneBy(i, Criteria(
		Equals(pk, id),
	))
}

// FindAll the rows
func (db *Database) FindAll(i interface{}) ([]interface{}, error) {
	qb := buildBaseQuery(i)
	q := qb.getSQL()

	return doSelect(db.DB, q, i)
}

// FindBy all that match the criteria
func (db *Database) FindBy(i interface{}, criteria []Clause) ([]interface{}, error) {
	qb := buildBaseQuery(i)
	q := buildCriteria(qb, criteria)

	return doSelect(db.DB, q, i)
}

// FindByOrdered all that match the criteria and order it
func (db *Database) FindByOrdered(i interface{}, criteria []Clause, ordering map[string]string) ([]interface{}, error) {
	qb := buildBaseQuery(i)
	buildOrdering(qb, ordering)
	q := buildCriteria(qb, criteria)

	return doSelect(db.DB, q, i)
}

// FindOneBy that matches the criteria
func (db *Database) FindOneBy(i interface{}, criteria []Clause) (interface{}, error) {
	qb := buildBaseQuery(i)
	qb.AddLimit(1)
	q := buildCriteria(qb, criteria)

	return OneResultOrNil(doSelect(db.DB, q, i))
}

// FindOneByOrdered that matches the criteria and order it
func (db *Database) FindOneByOrdered(i interface{}, criteria []Clause, ordering map[string]string) (interface{}, error) {
	qb := buildBaseQuery(i)
	qb.AddLimit(1)
	buildOrdering(qb, ordering)
	q := buildCriteria(qb, criteria)

	return OneResultOrNil(doSelect(db.DB, q, i))
}

// First returns the first result in the resultset
func (db *Database) First(i interface{}, criteria []Clause) (interface{}, error) {
	qb := buildBaseQuery(i)
	qb.AddOffset(0)
	qb.AddLimit(1)

	q := buildCriteria(qb, criteria)

	return OneResultOrNil(doSelect(db.DB, q, i))
}

// Count rows that matches the criteria
func (db *Database) Count(i interface{}, criteria []Clause) (int64, error) {
	qb := buildBaseQuery(i)
	qb.Select("count(id) as n")
	q := buildCriteria(qb, criteria)

	r, err := OneResultOrNil(doSelect(db.DB, q, Scalar{}))
	if err != nil {
		return 0, err
	}

	v := reflect.ValueOf(r)

	return v.FieldByName("N").Int(), nil
}

// Save handles a save action for a model
func (db *Database) Save(i interface{}) error {
	id, err := doInsert(db.DB, i)
	if err != nil {
		return err
	}

	rv := reflect.ValueOf(i)
	if rv.Type().Kind() == reflect.Ptr {
		rv = rv.Elem()
	}
	for j := 0; j < rv.NumField(); j++ {
		field := rv.Field(j)
		fieldType := rv.Type().Field(j)

		if !field.CanAddr() {
			return fmt.Errorf("field `%s` cannot be addressed", fieldType.Name)
		}

		if !field.CanSet() {
			return fmt.Errorf("field `%s` cannot be set", fieldType.Name)
		}

		if index, ok := fieldType.Tag.Lookup("index"); ok && index == "pk" {
			field.SetInt(id)
		}
	}

	return nil
}

// Update handles an update action for a model
func (db *Database) Update(i interface{}) error {
	pk, id := GetPrimaryKey(reflect.ValueOf(i))
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	_, err := db.UpdateBy(i, Criteria(
		Equals(pk, id),
	))
	if err != nil {
		return err
	}

	return nil
}

// UpdateFields only updates fields that are specified in the fields argument
func (db *Database) UpdateFields(i interface{}, fields []*Field) error {
	pk, id := GetPrimaryKey(reflect.ValueOf(i))
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	qb := NewQuery(UpdateStmt)
	qb.setTableName(GetTableName(i))
	qb.fields = fields
	q := buildCriteria(qb, Criteria(
		Equals(pk, id),
	))

	_, err := doUpdate(db.DB, q)
	if err != nil {
		return err
	}

	return nil
}

// UpdateBy a struct
func (db *Database) UpdateBy(i interface{}, criteria []Clause) (int64, error) {
	qb := NewQuery(UpdateStmt)
	qb.setTableName(GetTableName(i))
	qb.setFields(i)
	q := buildCriteria(qb, criteria)

	rowsAffected, err := doUpdate(db.DB, q)
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}

// DeleteBy a struct
func (db *Database) DeleteBy(i interface{}, criteria []Clause) (int64, error) {
	qb := NewQuery(DeleteStmt)
	qb.setTableName(GetTableName(i))
	qb.setFields(i)
	q := buildCriteria(qb, criteria)

	rowsAffected, err := doUpdate(db.DB, q)
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}

// OneToOne fetches a single object from the database that relate to the parent
func (db *Database) OneToOne(rv reflect.Value, c interface{}, join string) error {
	pk, id := GetPrimaryKey(rv)
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	object, err := db.FindOneBy(c, Criteria(
		Equals(pk, id),
	))
	if err != nil {
		return err
	}
	if object == nil {
		return nil
	}

	objectPointer := Pointer(object)
	db.Relations(objectPointer)

	rv.FieldByName(join).Set(objectPointer)

	return err
}

// OneToMany fetches multiple objects from the database that relate to the parent
func (db *Database) OneToMany(parent reflect.Value, child interface{}, mappedBy string) error {
	pk, id := GetPrimaryKey(parent)
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	objects, err := db.FindBy(child, Criteria(
		Equals(pk, id),
	))
	if err != nil {
		return err
	}

	for j := 0; j < len(objects); j++ {
		objectPointer := Pointer(objects[j])

		db.Relations(objectPointer)

		parent.FieldByName(mappedBy).Set(reflect.Append(parent.FieldByName(mappedBy), objectPointer))
	}

	return nil
}

// Relations fetches all the child objects from the database that are related to the parent
func (db *Database) Relations(parent reflect.Value) interface{} {
	if parent.Kind() == reflect.Ptr {
		parent = parent.Elem()
	}

	for i := 0; i < parent.NumField(); i++ {
		t := parent.Type().Field(i)
		if r, ok := t.Tag.Lookup("related"); ok {
			if parent.Field(i).Kind() == reflect.Ptr {
				db.OneToOne(parent, GetRegistryEntry(r), t.Name)
			} else if parent.Field(i).Kind() == reflect.Slice {
				db.OneToMany(parent, GetRegistryEntry(r), t.Name)
			}
		}
	}

	return parent.Interface()
}

// MarshalObject returns a JSON structure of the provided interface
func (db *Database) MarshalObject(i interface{}) (interface{}, error) {
	var rv = reflect.ValueOf(i)
	if rv.Kind() != reflect.Ptr {
		rv = Pointer(i)
	}
	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	jsonData := make(map[string]interface{})

	for i := 0; i < rv.NumField(); i++ {
		t := rv.Type().Field(i)
		f := rv.Field(i)
		d, hasDbName := t.Tag.Lookup("db")
		if !hasDbName || d == "-" {
			continue
		}

		_, hasIndex := t.Tag.Lookup("index")
		if hasIndex && f.Int() == 0 {
			continue
		}
		if gt, ok := f.Interface().(Time); ok && gt.IsZero() {
			continue
		}

		if m, ok := t.Tag.Lookup("cor"); ok {
			object, err := db.Find(GetRegistryEntry(m), rv.Field(i).Int())
			if err != nil {
				return nil, err
			}
			p := reflect.ValueOf(object)

			jsonData[d] = p.FieldByName("CorrelationID").Interface()
		} else {
			jsonData[d] = f.Interface()
		}
	}

	return jsonData, nil
}

// Pointer returns a new reference of provided interface and
// fills it with the data of the interface
func Pointer(i interface{}) reflect.Value {
	nv := reflect.New(reflect.TypeOf(i)).Interface()
	nrv := reflect.ValueOf(nv).Elem()
	rv := reflect.ValueOf(i)

	for j := 0; j < rv.NumField(); j++ {
		nrv.Field(j).Set(rv.Field(j))
	}

	return reflect.ValueOf(nv)
}

// GetPrimaryKey returns the primary key of a struct
func GetPrimaryKey(rv reflect.Value) (string, interface{}) {
	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	for i := 0; i < rv.NumField(); i++ {
		ft := rv.Type().Field(i)
		f := rv.Field(i)

		if t, ok := ft.Tag.Lookup("index"); ok && t == "pk" {
			return ft.Tag.Get("db"), f.Interface()
		}
	}

	return "", nil
}

// OneResultOrNil returns nil when the query returns empty
func OneResultOrNil(results []interface{}, err error) (interface{}, error) {
	if err != nil {
		return nil, err
	}
	if len(results) == 0 {
		return nil, nil
	}

	return results[0], nil
}

// GetTableName returns the table name based on the struct.
// Converts CamelCase naming to snake_case.
func GetTableName(i interface{}) string {
	reflectedInterface := reflect.ValueOf(i)
	if reflectedInterface.Kind() == reflect.Ptr {
		i = reflectedInterface.Elem().Interface()
	}

	interfaceType := reflect.TypeOf(i)
	interfaceName := Underscore(interfaceType.Name())

	return strings.ToLower(interfaceName)
}
