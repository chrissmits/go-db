package sonicdb

import (
	"testing"
)

func TestNow(t *testing.T) {
	now := Now()
	if now.IsZero() {
		t.Fatal("Zero date returned")
	}
}

func TestCreateTime(t *testing.T) {
	date, err := CreateTime("2018-06-10 22:00:00")
	if date.Format("2006-01-02 15:04:05") != "2018-06-10 22:00:00" || err != nil {
		t.Fatal("Failed to create time")
	}
}

func TestTime_Localized(t *testing.T) {
	date, _ := CreateTime("2018-06-10 15:00:00")

	expect := "2018-06-10 17:00:00"
	actual := date.Localized("2006-01-02 15:04:05")

	if expect != actual {
		t.Fatalf("Failed to localize date, got %v excepted %v", actual, expect)
	}
}

func TestTime_Sub(t *testing.T) {
	date1, _ := CreateTime("2018-06-10 15:00:00")
	date2, _ := CreateTime("2018-06-10 16:15:00")

	diff := date2.Sub(date1)

	expectHours := 1.25
	actualHours := diff.Hours()

	if expectHours != actualHours {
		t.Errorf("expected %v got %v", expectHours, actualHours)
	}

	expectMinutes := 75.00
	actualMinutes := diff.Minutes()

	if expectMinutes != actualMinutes {
		t.Errorf("expected %v got %v", expectMinutes, actualHours)
	}
}

func TestTime_MarshalJSON(t *testing.T) {
	//expect := []byte("2018-06-10 20:00:00")
	//
	//date, _ := CreateTime("2018-06-10 20:00:00")
	//actual, _ := date.MarshalJSON()
	//
	//if !reflect.DeepEqual(expect, actual) {
	//	t.Fatalf("expected %v got %v", expect, actual)
	//}
}
