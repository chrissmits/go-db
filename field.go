package sonicdb

import (
	"reflect"
)

// Field this struct holds all the data needed for the insert
type Field struct {
	Name        string
	Kind        reflect.Kind
	Value       interface{}
	Placeholder string
}

// getInterfaceFields the columns from the type that needs to be saved
func getInterfaceFields(value reflect.Value, onlyFields []string) []*Field {
	fields := make([]*Field, 0)

	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)
		fieldType := value.Type().Field(i)

		if _, ok := fieldType.Tag.Lookup("embedded"); ok {
			for j := 0; j < field.NumField(); j++ {
				embeddedField := field.Field(j)
				embeddedFieldType := field.Type().Field(j)

				if dbField := fromInterfaceField(embeddedField, embeddedFieldType, onlyFields); dbField != nil {
					fields = append(fields, dbField)
				}
			}
		} else {
			if dbField := fromInterfaceField(field, fieldType, onlyFields); dbField != nil {
				fields = append(fields, dbField)
			}
		}
	}

	return fields
}

func fromInterfaceField(field reflect.Value, fieldType reflect.StructField, onlyFields []string) *Field {
	// Check if db column name is defined, if not, skip it.
	columnName, ok := fieldType.Tag.Lookup("db")
	if !ok || (columnName == "" || columnName == "-") {
		return nil
	}

	// Check if column name is in onlyFields
	if onlyFields != nil && !contains(onlyFields, columnName) {
		return nil
	}

	// Check if index tag has been set, if 0, skip it.
	if indexTag, ok := fieldType.Tag.Lookup("index"); ok {
		if (indexTag == "pk" || indexTag == "fk") && field.Int() == 0 {
			return nil
		}
	}

	vf := field
	if t, ok := vf.Interface().(Time); ok {
		if _, ok := fieldType.Tag.Lookup("on_create"); ok && t.IsZero() {
			vf = reflect.ValueOf(Now())
		} else if _, ok := fieldType.Tag.Lookup("on_update"); ok {
			vf = reflect.ValueOf(Now())
		} else if t.IsZero() {
			return nil
		} else {
			vf = reflect.ValueOf(t.UTC())
		}
	}

	// Column definition
	dbField := new(Field)
	dbField.Name = columnName
	dbField.Kind = vf.Kind()
	dbField.Placeholder = ":" + columnName
	dbField.Value = vf.Interface()

	return dbField
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}

	return false
}
