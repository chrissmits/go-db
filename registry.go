package sonicdb

var registry map[string]interface{}

// SetRegistry fills the registry with symlinks to the models
func SetRegistry(r map[string]interface{}) {
	registry = r
}

// GetRegistryEntry returns a model from the registry
func GetRegistryEntry(name string) interface{} {
	entry, ok := registry[name]
	if !ok {
		panic("registry entry not found")
	}

	return entry
}
