module go-db

require (
	bitbucket.org/bluewisp/go-bundle v0.0.0-20171211145247-7ea058d06c04
	github.com/go-sql-driver/mysql v1.4.0
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
